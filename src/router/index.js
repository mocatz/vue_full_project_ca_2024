import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/movies',
      name: 'movies',
      component: () => import('../views/MovieView.vue')
    },
    {
      path: '/todo',
      name: 'todo',
      component: () => import('../views/TodoView.vue')
    },
    {
      path: '/quizzmaker',
      name: 'quizzmaker',
      component: () => import('../views/QuizzMaker.vue')
    },
    {
      path: '/quizzmaker/:id',
      name: 'quizzmakeredit',
      component: () => import('../views/QuizzMaker.vue')
    },
    {
      path: '/quizzformv2',
      name: 'quizzformv2',
      component: () => import('../views/QuizzFormV2.vue')
    },
    {
      path: '/quizzformv2/:id',
      name: 'quizzformv2Edit',
      component: () => import('../views/QuizzFormV2.vue')
    },
    {
      path: '/quizz',
      name: 'quizz',
      component: () => import('../views/QuizzView.vue')
    },
    {
      path: '/quizzstats',
      name: 'stats',
      component: () => import('../views/QuizzStats.vue')
    },
    {
      path: '/quizz/:id',
      name: 'quizzdetail',
      component: () => import('../views/QuizzDetail.vue')
    },
    {
      path: '/quizzlist/',
      name: 'quizzlist',
      component: () => import('../views/QuizzList.vue')
    },
    {
      path: '/quizzgame/:id',
      name: 'quizzgame',
      component: () => import('../views/QuizzGame.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
