import axios from './axiosInstance'

const ENDPOINT = 'quizz'

export const quizzAPI = {
  getQuizz() {
    return axios
      .get(ENDPOINT)
      .then((res) => {
        return res
      })
      .catch((err) => {
        console.error('ERROR ON GET QUIZZ: ', err)
      })
  },
  postQuizz(quizz) {
    return axios
      .post(ENDPOINT, quizz)
      .then((res) => {
        return res
      })
      .catch((err) => {
        console.error('ERROR ON POST QUIZZ: ', err)
      })
  },
  deleteQuizz(quizzId) {
    return axios
      .delete(`${ENDPOINT}/${quizzId}`)
      .then((res) => {
        return res
      })
      .catch((err) => {
        console.error('ERROR ON DELETE QUIZZ: ', err)
      })
  }
}
