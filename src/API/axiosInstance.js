import axios from 'axios'

const NODE_ENV_MODE = import.meta.env.MODE

export const APIURL = () => {
  if (NODE_ENV_MODE == 'production') {
    return 'http://localhost:3000/'
  }

  return 'http://localhost:3000/'
}

export default axios.create({
  baseURL: APIURL()
})
