import axios from './axiosInstance'

const ENDPOINT = 'netflix'

export const movieAPI = {
  getMovies() {
    return axios
      .get(ENDPOINT)
      .then((res) => {
        return res
      })
      .catch((err) => {
        console.error('ERROR ON FETCH MOVIES: ', err)
      })
  }
}
