import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useQuizzV2Store = defineStore(
  'quizzV2',
  () => {
    const quizzList = ref([])

    function addQuizz(quizz) {
      quizzList.value.push(quizz)
    }
    function editQuizz(quizz) {
      const idxMatch = quizzList.value.findIndex((elm) => elm.id == quizz.id)
      quizzList.value[idxMatch] = quizz
    }

    return { quizzList, addQuizz, editQuizz }
  },
  {
    persist: true
  }
)
