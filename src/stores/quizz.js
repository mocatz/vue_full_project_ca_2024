import { ref } from 'vue'
import { defineStore } from 'pinia'
import { quizzAPI } from '../API/quizz.js'

/**
 * -----------------------------------------------------------------------------
 *                               QUIZZ MODEL
 * -----------------------------------------------------------------------------
 
  {
    id: String, // UUID
    title: String,
    description: String,
    questions: [
      {
        question: String,
        answers: [
          {
            answer: String,
            checked: Boolean
          },
        ]
      },
    ]
  }

 * 
 */

export const useQuizzStore = defineStore(
  'quizz',
  () => {
    const quizz = ref([])
    const quizzLoading = ref(false)
    const results = ref([])

    function addQuizz(quizzToAdd) {
      // -- USE ONLY WITH THE STORE
      // quizz.value.push(quizzToAdd)

      // -- USE WITH API SERVER
      quizzLoading.value = true
      quizzAPI
        .postQuizz(quizzToAdd)
        .then((response) => {
          console.log('Quizz posted: ', response)
          quizz.value.push(quizzToAdd)
        })
        .catch((err) => {
          console.error('Error on post quizz: ', err)
        })
        .finally(() => {
          quizzLoading.value = false
        })
    }

    function fetchQuizz() {
      quizzAPI
        .getQuizz()
        .then((response) => {
          console.log('fetchQuizz response: ', response)
          quizz.value = response.data
        })
        .catch((err) => {
          console.error('ERROR ON FETCH QUIZZ: ', err)
        })
    }

    function addResult(result) {
      results.value.push(result)
    }

    function deleteQuizz(quizzId) {
      // -- USE ONLY WITH THE STORE
      // quizz.value = quizz.value.filter((elm) => elm.id !== quizzId)

      // -- USE WITH API SERVER
      quizzAPI
        .deleteQuizz(quizzId)
        .then((response) => {
          console.log('Delete Quizz response: ', response)
          quizz.value = quizz.value.filter((elm) => elm.id !== quizzId)
        })
        .catch((err) => {
          console.error('ERROR ON DELETE QUIZZ: ', err)
        })
    }

    function editQuizz(quizz) {
      const idxMatch = quizz.value.findIndex((elm) => elm.id == quizz.id)
      quizz.value[idxMatch] = quizz
    }

    return { quizz, results, addQuizz, addResult, deleteQuizz, editQuizz, fetchQuizz }
  },
  {
    persist: true
  }
)
