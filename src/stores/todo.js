import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useTodoStore = defineStore(
  'todo',
  () => {
    const todo = ref([])

    function addTodo(task) {
      todo.value.push(task)
    }
    function updateStatus(taskId) {
      const taskIdx = todo.value.findIndex((elm) => elm.id == taskId)
      if (taskIdx >= 0) {
        const newStatus = todo.value[taskIdx].status == 'TODO' ? 'DONE' : 'TODO'
        if (newStatus == 'DONE') {
          todo.value[taskIdx].closed = new Date().getTime()
        }
        todo.value[taskIdx].status = newStatus
      }
    }
    function deleteTask(taskId) {
      const taskIdx = todo.value.findIndex((elm) => elm.id == taskId)
      if (taskIdx >= 0) {
        todo.value = todo.value.filter((elm) => elm.id !== taskId)
      }
    }

    return { todo, addTodo, updateStatus, deleteTask }
  },
  {
    persist: true
  }
)
