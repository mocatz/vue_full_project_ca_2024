import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { movieAPI } from '../API/movies.js'

export const useMovieStore = defineStore('movie', () => {
  const movies = ref([])
  const loadingMovies = ref(false)

  const first200 = computed(() => {
    // return movies.value.slice(2)
    return movies.value.length > 200 ? movies.value.filter((elm, idx) => idx < 200) : []
  })

  const movieType = computed(() => {
    return Array.from(new Set(movies.value.map((elm) => elm.type)))
  })

  async function fetchMovies() {
    loadingMovies.value = true
    try {
      const response = await movieAPI.getMovies()
      movies.value = response.data
    } catch (error) {
      console.error('Error fetchMovies', error)
    } finally {
      loadingMovies.value = false
    }
  }

  return { movies, loadingMovies, fetchMovies, first200, movieType }
})
